import { Navigate } from "react-router-dom";
import "../index.css";
import { useGetTokenQuery } from "../state/authApi";
import { useState } from "react";
import { useGetMessagesQuery } from "../state/messagesApi";

export default function MessageBox({ list_id }) {
  const { data: token } = useGetTokenQuery({});
  const [message, setMessage] = useState("");
  const { data: messages } = useGetMessagesQuery(
    {
      list_id,
      user_id: token?.account.id,
    },
    { skip: !token || !list_id }
  );

  function sendMessage(e) {
    e.preventDefault();
    console.log(message);
  }
  console.log(messages);

  return (
    <div className="flex flex-col pt-4">
      <h1>Messages</h1>
      {messages?.length > 0 ? (
        messages.map((message) => (
          <MessageRow message={message} key={message.id} />
        ))
      ) : (
        <p>No messages</p>
      )}
      <form className="flex gap-4" onSubmit={sendMessage}>
        <label htmlFor="newMessage" hidden>
          New Message
        </label>
        <input
          type="text"
          id="newMessage"
          className="border border-green-600 rounded-sm"
          onChange={(e) => setMessage(e.target.value)}
        />
        <button
          type="submit"
          className="font-semibold text-sm hover:bg-emerald-500 px-2 py-1 rounded-sm bg-emerald-600 text-emerald-50 active:bg-emerald-800"
        >
          Send
        </button>
      </form>
    </div>
  );
}

function MessageRow({ message }) {
  const when = new Date(message.sent_at);
  return (
    <div className="border rounded-lg">
      <div className="flex pt-4 justify-between pb border-b border-b-slate-500 bg-slate-200">
        <p className="text-sm">{message.sent_by}</p>
        <p className="font-light text-xs">{when.toLocaleString()}</p>
      </div>
      <p className="pl-2">{message.body}</p>
    </div>
  );
}
