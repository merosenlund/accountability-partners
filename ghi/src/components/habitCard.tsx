import { Form, useSubmit } from "react-router-dom";
import "../index.css";
import { PlusCircleIcon } from "@heroicons/react/20/solid";
import MessageBox from "./messageBox";

export default function HabitCard({ habitList }) {
  const submit = useSubmit();

  const handleSubmit = (e) => {
    e.preventDefault();
    submit(e.currentTarget);
    e.currentTarget.form.reset();
  };

  return (
    <div key={habitList.id} className="p-4">
      <h3 className="text-xl font-semibold">{habitList.name}</h3>
      <Form method="post" className="flex">
        <input name="list_id" type="hidden" value={habitList.id} />
        <input
          type="text"
          name="description"
          className="border border-emerald-500 mr-2 px-2"
          placeholder="Add habit"
        />
        <button
          type="submit"
          name="intent"
          value="addHabit"
          onClick={handleSubmit}
        >
          <PlusCircleIcon className="h-6 w-6 hover:text-emerald-500 text-emerald-600 text-emerald-50 active:text-emerald-800" />
        </button>
      </Form>
      {habitList.habits.length ? (
        <ul>
          {habitList.habits.map((habit) => (
            <li key={habit.id}>{habit.description}</li>
          ))}
        </ul>
      ) : (
        <p>No habits in list</p>
      )}
      <MessageBox list_id={habitList.id} />
    </div>
  );
}
