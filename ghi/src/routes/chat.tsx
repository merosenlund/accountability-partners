import { Navigate } from "react-router-dom";
import "../index.css";
import { useGetTokenQuery } from "../state/authApi";
import { useEffect, useState } from "react";

export default function Chat() {
  const { data: token } = useGetTokenQuery({});
  const [messages, setMessages] = useState([]);
  const [connected, setConnected] = useState(false);
  const [message, setMessage] = useState("");
  const [loading, setLoading] = useState(false);
  const [socket, setSocket] = useState<WebSocket | null>(null);

  function connect() {
    if (loading || connected) {
      return;
    }
    setLoading(true);
    // Should be an environment variable in the future
    const url = `ws://localhost:8000/chat/61/${token.account.id}`;
    setSocket(new WebSocket(url));
  }

  useEffect(() => {
    if (socket) {
      socket.addEventListener("open", () => {
        setConnected(true);
        setLoading(false);
      });
      socket.addEventListener("close", () => {
        setConnected(false);
        setLoading(false);
        setTimeout(() => {
          connect();
        }, 1000);
      });
      socket.addEventListener("error", () => {
        setConnected(false);
        setLoading(false);
        setTimeout(() => {
          connect();
        }, 1000);
      });
      socket.addEventListener("message", (message) => {
        const newMessage = JSON.parse(message.data);
        console.log(newMessage);
        if (newMessage.message_type === "message") {
          setMessages((m) => [newMessage, ...m]);
        }
      });
    }
  }, [socket]);

  useEffect(() => {
    connect();
  }, []);

  function sendMessage(e) {
    e.preventDefault();
    socket.send(message);
  }

  return (
    <div className="flex flex-col items-center">
      {!token && <Navigate to="/login" />}
      <h1>Chat</h1>
      <form className="flex gap-4" onSubmit={sendMessage}>
        <label htmlFor="newMessage" hidden>
          New Message
        </label>
        <input
          type="text"
          id="newMessage"
          className="border border-green-600 rounded-sm"
          onChange={(e) => setMessage(e.target.value)}
        />
        <button
          type="submit"
          className="font-semibold text-sm hover:bg-emerald-500 px-2 py-1 rounded-sm bg-emerald-600 text-emerald-50 active:bg-emerald-800"
        >
          Send
        </button>
      </form>
      <table>
        <tbody>
          {messages.map((message) => (
            <MessageRow message={message} key={message.message_id} />
          ))}
        </tbody>
      </table>
    </div>
  );
}

function MessageRow({ message }) {
  const when = new Date(message.timestamp);
  return (
    <tr>
      <td>{message.client_id}</td>
      <td>{when.toLocaleString()}</td>
      <td>{message.body}</td>
    </tr>
  );
}
