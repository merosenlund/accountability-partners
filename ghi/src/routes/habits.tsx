import {
  Form,
  Link,
  Navigate,
  useRouteLoaderData,
  useSubmit,
} from "react-router-dom";
import "../index.css";
import { habitsApi, useGetHabitListsQuery } from "../state/habitsApi";
import { useGetTokenQuery } from "../state/authApi";
import Masonry from "react-responsive-masonry";
import { PlusCircleIcon } from "@heroicons/react/20/solid";
import HabitCard from "../components/habitCard";

export const loader = (store) => async () => {
  return (
    habitsApi.endpoints.getHabitLists.select({})(store.getState()).data ??
    (await store.dispatch(habitsApi.endpoints.getHabitLists.initiate({})))
  );
};

export const action =
  (store) =>
  async ({ request, ...rest }) => {
    const formData = await request.formData();
    const updates = Object.fromEntries(formData);
    const intent = updates.intent;
    let result;
    if (intent === "addHabitList") {
      result = await store.dispatch(
        habitsApi.endpoints.addHabitList.initiate(updates)
      );
    } else if (intent === "addHabit") {
      result = await store.dispatch(
        habitsApi.endpoints.addHabit.initiate(updates)
      );
    }
    return result;
  };

export default function Habits() {
  const { data: token } = useGetTokenQuery({});
  const { data: habitLists, isLoading } = useGetHabitListsQuery({});
  const submit = useSubmit();

  const handleSubmit = (e) => {
    e.preventDefault();
    submit(e.currentTarget);
    e.currentTarget.form.reset();
  };

  return (
    <div className="flex flex-col items-center">
      {!token && <Navigate to="/login" />}
      <h2 className="text-2xl font-semibold mt-8 mb-4">Habits</h2>
      <Form method="post" className="">
        <input
          type="text"
          name="name"
          className="border border-emerald-500 mr-4"
        />
        <button
          type="submit"
          className="font-semibold text-sm hover:bg-emerald-500 px-2 py-1 rounded-sm bg-emerald-600 text-emerald-50 active:bg-emerald-800"
          name="intent"
          value="addHabitList"
          onClick={handleSubmit}
        >
          Add List
        </button>
      </Form>
      <Masonry columnsCount={3}>
        {!isLoading &&
          habitLists.map((habitList) => (
            <HabitCard key={habitList.id} habitList={habitList} />
          ))}
      </Masonry>
    </div>
  );
}
