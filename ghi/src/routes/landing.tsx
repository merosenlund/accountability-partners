import { Form, Link } from "react-router-dom";

export default function Landing() {
  return (
    <div>
      <div className="flex justify-between bg-emerald-900 text-emerald-100 px-4">
        <h1 className="text-lg font-semibold">Habit Buddy</h1>
        <nav>
          <ul className="flex gap-4">
            <li>
              <Link to={`/about-us`}>About Us</Link>
            </li>
            <li>
              <Link to={`/pricing`}>Pricing</Link>
            </li>
          </ul>
        </nav>
      </div>
      <div className="px-4">
        <p>
          Change your life for the better and find a like minded buddy in the
          process!
        </p>
      </div>
    </div>
  );
}
