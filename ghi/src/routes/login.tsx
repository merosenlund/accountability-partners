import { Form, Navigate, redirect } from "react-router-dom";
import { authApi, useGetTokenQuery } from "../state/authApi";

export const loader = (store) => async () => {
  const token = authApi.endpoints.getToken.select({})(store.getState()).data;
  if (token) {
    return redirect("/habits");
  } else {
    return await store.dispatch(authApi.endpoints.getToken.initiate({}));
  }
};

export const action =
  (store) =>
  async ({ request }) => {
    const body = await request.formData();
    const result = await store.dispatch(authApi.endpoints.login.initiate(body));
    console.log("result", result);
    const token = authApi.endpoints.getToken.select({})(store.getState()).data;
    console.log("token", token);
    if (result.data) {
      return redirect("/habits");
    } else {
      return result;
    }
  };

export function Login() {
  const { data: token } = useGetTokenQuery({});

  return (
    <>
      {token && <Navigate to="/habits" />}
      <div className="flex items-center flex-col">
        <h2 className="text-2xl font-semibold mt-8">Login</h2>
        <Form method="post" className="flex flex-col items-center">
          <label
            htmlFor="username"
            className="self-start text-xs font-light text-gray-500 mt-8"
          >
            USERNAME
          </label>
          <input
            id="username"
            type="text"
            name="username"
            value="string"
            className="border border-emerald-500 px-2 rounded-sm"
          />
          <label
            htmlFor="password"
            className="self-start text-xs text-gray-500 mt-4"
          >
            PASSWORD
          </label>
          <input
            id="password"
            type="password"
            name="password"
            value="string"
            className="border border-emerald-500 px-2 rounded-sm"
          />
          <button
            type="submit"
            className="mt-8 font-semibold text-sm hover:bg-emerald-500 p-2 rounded-sm bg-emerald-600 text-emerald-50 active:bg-emerald-800"
          >
            LOGIN
          </button>
        </Form>
      </div>
    </>
  );
}
