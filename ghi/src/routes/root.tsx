import { Form, Link, Navigate, Outlet, useLoaderData } from "react-router-dom";
import { authApi, useGetTokenQuery } from "../state/authApi";
import { redirect } from "react-router-dom";

export const loader = (store) => async () => {
  return (
    authApi.endpoints.getToken.select({})(store.getState()).data ??
    (await store.dispatch(authApi.endpoints.getToken.initiate({})))
  );
};

export const action =
  (store) =>
  async ({ request }) => {
    const result = await store.dispatch(authApi.endpoints.logout.initiate({}));
    return redirect("/login");
  };

export default function Root() {
  const { data: token } = useGetTokenQuery({});
  return (
    <>
      <div className="flex justify-between bg-emerald-900 text-emerald-100 px-4">
        <h1 className="text-lg font-semibold">
          Habit Buddy {token?.account.name}
        </h1>
        <nav>
          <ul className="flex gap-4">
            {!token && (
              <>
                <li>
                  <Link to={`/`}>Landing Page</Link>
                </li>
                <li>
                  <Link to={`/login`}>Login</Link>
                </li>
              </>
            )}
            {token && (
              <>
                <li>
                  <Link to={`/habits`}>Habits</Link>
                </li>
                <li>
                  <Link to={`/chat`}>Chat</Link>
                </li>
                <li>
                  <Form method="delete">
                    <button type="submit">Logout</button>
                  </Form>
                </li>
              </>
            )}
          </ul>
        </nav>
      </div>
      <div id="detail" className="px-4">
        <Outlet />
      </div>
    </>
  );
}
