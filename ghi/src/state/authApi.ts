import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react";

export const authApi = createApi({
  reducerPath: "auth",
  baseQuery: fetchBaseQuery({ baseUrl: import.meta.env.VITE_API_HOST }),
  refetchOnFocus: true,
  tagTypes: ["token", "user"],
  endpoints: (builder) => ({
    login: builder.mutation({
      query: (credentials) => ({
        url: "/token",
        method: "POST",
        body: credentials,
        credentials: "include",
        contentType: "application/x-www-form-urlencoded",
      }),
      invalidatesTags: ["token", "user"],
    }),
    logout: builder.mutation({
      query: () => ({
        url: "/token",
        method: "delete",
        credentials: "include",
      }),
      invalidatesTags: ["token", "user"],
    }),
    getToken: builder.query({
      query: () => ({
        url: "/token",
        credentials: "include",
      }),
      providesTags: ["token"],
    }),
  }),
});

export const { useGetTokenQuery } = authApi;
