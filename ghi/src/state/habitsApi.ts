import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react";

export const habitsApi = createApi({
  reducerPath: "habits",
  baseQuery: fetchBaseQuery({ baseUrl: import.meta.env.VITE_API_HOST }),
  refetchOnFocus: true,
  tagTypes: ["habits", "habitLists"],
  endpoints: (builder) => ({
    getHabits: builder.query({
      query: () => "/habits",
      providesTags: ["habits"],
    }),
    addHabit: builder.mutation({
      query: (habit) => ({
        url: "/habits",
        method: "POST",
        body: habit,
      }),
      invalidatesTags: ["habits", "habitLists"],
    }),
    getHabitLists: builder.query({
      query: () => "/lists",
      providesTags: ["habitLists"],
    }),
    addHabitList: builder.mutation({
      query: (habitList) => ({
        url: "/lists",
        method: "POST",
        body: habitList,
        credentials: "include",
      }),
      invalidatesTags: ["habitLists"],
    }),
  }),
});

export const { useGetHabitsQuery, useGetHabitListsQuery } = habitsApi;
