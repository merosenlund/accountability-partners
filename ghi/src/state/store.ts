import { configureStore } from "@reduxjs/toolkit";
import { habitsApi } from "./habitsApi";
import { authApi } from "./authApi";
import { setupListeners } from "@reduxjs/toolkit/dist/query";
import { messagesApi } from "./messagesApi";

export const store = configureStore({
  reducer: {
    [habitsApi.reducerPath]: habitsApi.reducer,
    [authApi.reducerPath]: authApi.reducer,
    [messagesApi.reducerPath]: messagesApi.reducer,
  },
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware()
      .concat(habitsApi.middleware)
      .concat(authApi.middleware)
      .concat(messagesApi.middleware),
});

setupListeners(store.dispatch);
