import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react";

let socket: WebSocket | null = null;
function getSocket(list_id, user_id) {
  if (!socket) {
    socket = new WebSocket(
      `ws://localhost:8000/messages/${list_id}/${user_id}`
    );
  }
  return socket;
}

export const messagesApi = createApi({
  reducerPath: "messages",
  baseQuery: fetchBaseQuery({ baseUrl: import.meta.env.VITE_API_HOST }),
  refetchOnFocus: true,
  tagTypes: ["messages"],
  endpoints: (builder) => ({
    getMessages: builder.query({
      query: ({ list_id }) => `messages/${list_id}`,
      async onCacheEntryAdded(
        { list_id, user_id },
        { updateCachedData, cacheDataLoaded, cacheEntryRemoved }
      ) {
        // create a websocket connection when the cache subscription starts
        const ws = getSocket(list_id, user_id);
        try {
          // wait for the initial query to resolve before proceeding
          await cacheDataLoaded;

          // when data is received from the socket connection to the server,
          // if it is a message and for the appropriate channel,
          // update our query result with the received message
          const listener = (event) => {
            const data = JSON.parse(event.data);
            if (data.channel !== list_id) return;
            console.log(data);
            updateCachedData((draft) => {
              draft.push(data.content);
            });
          };

          ws.addEventListener("message", listener);
        } catch (err) {
          console.log(err);
          // no-op in case `cacheEntryRemoved` resolves before `cacheDataLoaded`,
          // in which case `cacheDataLoaded` will throw
        }
        // cacheEntryRemoved will resolve when the cache subscription is no longer active
        await cacheEntryRemoved;
        // perform cleanup steps once the `cacheEntryRemoved` promise resolves
        ws.close();
      },
    }),
  }),
});

export const { useGetMessagesQuery } = messagesApi;
