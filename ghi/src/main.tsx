import React from "react";
import ReactDOM from "react-dom/client";
import { createBrowserRouter, RouterProvider } from "react-router-dom";
import Root from "./routes/root";
import "./index.css";
import ErrorPage from "./error-page";
import Landing from "./routes/landing";
import Habits from "./routes/habits";
import { loader as rootLoader, action as rootAction } from "./routes/root";
import {
  loader as habitsLoader,
  action as habitsAction,
} from "./routes/habits";
import {
  Login,
  action as loginAction,
  loader as loginLoader,
} from "./routes/login";
import { Provider } from "react-redux";
import { store } from "./state/store";
import Chat from "./routes/chat";

const router = createBrowserRouter([
  {
    id: "root",
    path: "/",
    element: <Root />,
    errorElement: <ErrorPage />,
    loader: rootLoader(store),
    action: rootAction(store),
    children: [
      {
        path: "/landing",
        element: <Landing />,
      },
      {
        path: "/habits",
        element: <Habits />,
        loader: habitsLoader(store),
        action: habitsAction(store),
      },
      {
        path: "/login",
        element: <Login />,
        action: loginAction(store),
        loader: loginLoader(store),
      },
      {
        path: "/chat",
        element: <Chat />,
      },
    ],
  },
]);

ReactDOM.createRoot(document.getElementById("root") as HTMLElement).render(
  <React.StrictMode>
    <Provider store={store}>
      <RouterProvider router={router} />
    </Provider>
  </React.StrictMode>
);
