## Endpoints

### Authentication

- Create user
- Sign in
- Sign out

### Habits

- Get all passwords for user
- Get all habits that need a partner
- Create a habit
- Add partner to habit
- Mark habit as complete for day (or whatever the repeat level is)
- Mark habit as fulfilled

### Messages

- I want to use websockets so I will fill this out more when I get a better idea of what this will take

Eventually each endpoint should be documented using the following format:

### «Human-readable of the endpoint»

- Endpoint path: «path to use»
- Endpoint method: «HTTP method»
- Query parameters:

  - «name»: «purpose»

- Headers:

  - Authorization: Bearer token

- Request shape (JSON):

  ```json
  «JSON-looking thing that has the
  keys and types in it»
  ```

- Response: «Human-readable description
  of response»
- Response shape (JSON):
  ```json
  «JSON-looking thing that has the
  keys and types in it»
  ```
