from datetime import datetime
from fastapi import HTTPException
from pydantic import BaseModel
from queries.pool import pool
from psycopg.rows import dict_row


class MessageIn(BaseModel):
    body: str
    sent_by_id: int
    list_id: int


class MessageOut(BaseModel):
    id: int
    body: str
    sent_at: datetime
    sent_by: str
    list_id: int
    viewed: bool


class Message(BaseModel):
    id: int
    body: str
    sent_at: datetime
    sent_by: int
    list_id: int
    viewed: bool


class MessageRepository:
    def create(self, message: MessageIn) -> MessageOut:
        try:
            with pool.connection() as conn:
                with conn.cursor(row_factory=dict_row) as db:
                    db.execute(
                        """
                        INSERT INTO message
                            (body, sent_by, list_id)
                        VALUES
                            (%s, %s, %s)
                        RETURNING *;
                        """,
                        [message.body, message.sent_by_id, message.list_id],
                    )
                    return MessageOut(**db.fetchone())
        except Exception as e:
            print(type(e))
            raise HTTPException(
                status_code=500,
                detail="Something went wrong, please try again later",
            )

    def get_all(self, list_id: int) -> list[MessageOut]:
        try:
            with pool.connection() as conn:
                with conn.cursor(row_factory=dict_row) as db:
                    db.execute(
                        """
                        SELECT m.id,
                               m.body,
                               m.sent_at,
                               m.list_id,
                               m.viewed,
                               a.username sent_by
                        FROM message m
                        JOIN account a ON m.sent_by = a.id
                        WHERE m.list_id = %s;
                        """,
                        [list_id],
                    )
                    raw_messages = db.fetchall()
                    print("&&&&&&&&&&&&&&&&&&&&&&", raw_messages)
                    return [MessageOut(**row) for row in raw_messages]
        except Exception as e:
            print(type(e))
            raise HTTPException(
                status_code=500,
                detail="Something went wrong, please try again later",
            )

    def get(self, username: str) -> Message:
        try:
            with pool.connection() as conn:
                with conn.cursor(row_factory=dict_row) as db:
                    db.execute(
                        """
                        SELECT * FROM message
                        WHERE username = %s;
                        """,
                        [username],
                    )
                    return Message(**db.fetchone())
        except Exception as e:
            print(type(e))
            raise HTTPException(
                status_code=500,
                detail="Something went wrong, please try again later",
            )

    def get_one(self, username: int) -> MessageOut:
        try:
            with pool.connection() as conn:
                with conn.cursor(row_factory=dict_row) as db:
                    db.execute(
                        """
                        SELECT * FROM message
                        WHERE id = %s;
                        """,
                        [id],
                    )
                    return MessageOut(**db.fetchone())
        except Exception as e:
            print(type(e))
            raise HTTPException(
                status_code=500,
                detail="Something went wrong, please try again later",
            )

    def update(self, id: int, message: MessageIn) -> MessageOut:
        habit_data = message.dict()
        values = list(habit_data.values())
        with pool.connection() as conn:
            try:
                with conn.cursor(row_factory=dict_row) as db:
                    db.execute(
                        """
                        UPDATE message
                        SET username = %s
                            , hashed_password = %s
                            , email = %s
                        WHERE id = %s
                        RETURNING *;
                        """,
                        [*values, id],
                    )
                    result = db.fetchone()
                    if result is None:
                        raise HTTPException(
                            status_code=404, detail="Habit not found"
                        )
                        return MessageOut(**db.fetchone())
            except Exception as e:
                print(type(e))
                raise HTTPException(
                    status_code=404,
                    detail="Update failed, please try again later",
                )

    def delete(self, id: int) -> bool:
        try:
            with pool.connection() as conn:
                with conn.cursor(row_factory=dict_row) as db:
                    db.execute(
                        """
                        DELETE FROM message
                        WHERE id = %s;
                        """,
                        [id],
                    )
                    if db.rowcount == 0:
                        raise HTTPException(
                            status_code=404, detail="Habit not found"
                        )
                    return True
        except Exception as e:
            print(type(e))
            raise HTTPException(
                status_code=500,
                detail="Something went wrong, please try again later",
            )
