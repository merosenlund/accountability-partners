from fastapi import HTTPException
from pydantic import BaseModel
from queries.pool import pool
from psycopg.rows import dict_row


class HabitIn(BaseModel):
    description: str
    list_id: int


class HabitOut(BaseModel):
    id: int
    description: str


class Habit(BaseModel):
    id: int
    description: str


class HabitRepository:
    def create(self, habit: HabitIn) -> HabitOut:
        try:
            with pool.connection() as conn:
                with conn.cursor(row_factory=dict_row) as db:
                    db.execute(
                        """
                        INSERT INTO habit
                            (description, list_id)
                        VALUES
                            (%s, %s)
                        RETURNING *;
                        """,
                        [habit.description, habit.list_id],
                    )
                    return HabitOut(**db.fetchone())
        except Exception as e:
            print(type(e))
            raise HTTPException(
                status_code=500,
                detail="Something went wrong, please try again later",
            )

    def get_all(self) -> list[HabitOut]:
        try:
            with pool.connection() as conn:
                with conn.cursor(row_factory=dict_row) as db:
                    db.execute(
                        """
                        SELECT id, description
                        FROM habit;
                        """
                    )
                    return [HabitOut(**row) for row in db.fetchall()]
        except Exception as e:
            print(type(e))
            raise HTTPException(
                status_code=500,
                detail="Something went wrong, please try again later",
            )

    def get(self, username: str) -> Habit:
        try:
            with pool.connection() as conn:
                with conn.cursor(row_factory=dict_row) as db:
                    db.execute(
                        """
                        SELECT * FROM habit
                        WHERE username = %s;
                        """,
                        [username],
                    )
                    return Habit(**db.fetchone())
        except Exception as e:
            print(type(e))
            raise HTTPException(
                status_code=500,
                detail="Something went wrong, please try again later",
            )

    def get_one(self, username: int) -> HabitOut:
        try:
            with pool.connection() as conn:
                with conn.cursor(row_factory=dict_row) as db:
                    db.execute(
                        """
                        SELECT * FROM habit
                        WHERE id = %s;
                        """,
                        [id],
                    )
                    return HabitOut(**db.fetchone())
        except Exception as e:
            print(type(e))
            raise HTTPException(
                status_code=500,
                detail="Something went wrong, please try again later",
            )

    def update(self, id: int, habit: HabitIn) -> HabitOut:
        habit_data = habit.dict()
        values = list(habit_data.values())
        with pool.connection() as conn:
            try:
                with conn.cursor(row_factory=dict_row) as db:
                    db.execute(
                        """
                        UPDATE habit
                        SET username = %s
                            , hashed_password = %s
                            , email = %s
                        WHERE id = %s
                        RETURNING *;
                        """,
                        [*values, id],
                    )
                    result = db.fetchone()
                    if result is None:
                        raise HTTPException(
                            status_code=404, detail="Habit not found"
                        )
                        return HabitOut(**db.fetchone())
            except Exception as e:
                print(type(e))
                raise HTTPException(
                    status_code=404,
                    detail="Update failed, please try again later",
                )

    def delete(self, id: int) -> bool:
        try:
            with pool.connection() as conn:
                with conn.cursor(row_factory=dict_row) as db:
                    db.execute(
                        """
                        DELETE FROM habit
                        WHERE id = %s;
                        """,
                        [id],
                    )
                    if db.rowcount == 0:
                        raise HTTPException(
                            status_code=404, detail="Habit not found"
                        )
                    return True
        except Exception as e:
            print(type(e))
            raise HTTPException(
                status_code=500,
                detail="Something went wrong, please try again later",
            )
