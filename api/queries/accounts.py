from fastapi import HTTPException
from pydantic import BaseModel
from typing import Optional
from queries.pool import pool
from psycopg.errors import UniqueViolation
from psycopg.rows import dict_row


class AccountIn(BaseModel):
    username: str
    password: str
    email: Optional[str]


class AccountOut(BaseModel):
    id: int
    username: str
    email: Optional[str]


class Account(BaseModel):
    id: int
    username: str
    email: Optional[str]
    hashed_password: str


class AccountRepository:
    def create(self, account: AccountIn, hashed_password) -> AccountOut:
        try:
            with pool.connection() as conn:
                with conn.cursor(row_factory=dict_row) as db:
                    db.execute(
                        """
                        INSERT INTO account
                            (username, hashed_password, email)
                        VALUES
                            (%s, %s, %s)
                        RETURNING *;
                        """,
                        [account.username, hashed_password, account.email],
                    )
                    return AccountOut(**db.fetchone())
        except UniqueViolation:
            raise HTTPException(
                status_code=400, detail="Username already exists"
            )
        except Exception as e:
            # Print out the type of a the exception
            # so we know what we need to handle
            print(type(e))
            raise HTTPException(
                status_code=500,
                detail="Something went wrong, please try again later",
            )

    def get_all(self) -> list[AccountOut]:
        try:
            with pool.connection() as conn:
                with conn.cursor(row_factory=dict_row) as db:
                    db.execute(
                        """
                        SELECT id, username, email
                        FROM account;
                        """
                    )
                    return [AccountOut(**row) for row in db.fetchall()]
        except Exception as e:
            print(type(e))
            raise HTTPException(
                status_code=500,
                detail="Something went wrong, please try again later",
            )

    def get(self, username: str) -> Account:
        try:
            with pool.connection() as conn:
                with conn.cursor(row_factory=dict_row) as db:
                    db.execute(
                        """
                        SELECT * FROM account
                        WHERE username = %s;
                        """,
                        [username],
                    )
                    return Account(**db.fetchone())
        except Exception as e:
            print(type(e))
            raise HTTPException(
                status_code=500,
                detail="Something went wrong, please try again later",
            )

    def get_one(self, username: int) -> AccountOut:
        try:
            with pool.connection() as conn:
                with conn.cursor(row_factory=dict_row) as db:
                    db.execute(
                        """
                        SELECT * FROM account
                        WHERE id = %s;
                        """,
                        [id],
                    )
                    return AccountOut(**db.fetchone())
        except Exception as e:
            print(type(e))
            raise HTTPException(
                status_code=500,
                detail="Something went wrong, please try again later",
            )

    def update(self, id: int, account: AccountIn) -> AccountOut:
        account_data = account.dict()
        values = list(account_data.values())
        with pool.connection() as conn:
            try:
                with conn.cursor(row_factory=dict_row) as db:
                    db.execute(
                        """
                        UPDATE account
                        SET username = %s
                            , hashed_password = %s
                            , email = %s
                        WHERE id = %s
                        RETURNING *;
                        """,
                        [*values, id],
                    )
                    result = db.fetchone()
                    if result is None:
                        raise HTTPException(
                            status_code=404, detail="Account not found"
                        )
                        return AccountOut(**db.fetchone())
            except Exception as e:
                print(type(e))
                raise HTTPException(
                    status_code=404,
                    detail="Update failed, please try again later",
                )

    def delete(self, id: int) -> bool:
        try:
            with pool.connection() as conn:
                with conn.cursor(row_factory=dict_row) as db:
                    db.execute(
                        """
                        DELETE FROM account
                        WHERE id = %s;
                        """,
                        [id],
                    )
                    if db.rowcount == 0:
                        raise HTTPException(
                            status_code=404, detail="Account not found"
                        )
                    return True
        except Exception as e:
            print(type(e))
            raise HTTPException(
                status_code=500,
                detail="Something went wrong, please try again later",
            )
