from fastapi import HTTPException
from pydantic import BaseModel
from queries.pool import pool
from psycopg.rows import dict_row
from .habits import HabitOut


class HabitListIn(BaseModel):
    name: str


class HabitListOut(BaseModel):
    id: int
    name: str


class HabitListOutWithHabits(BaseModel):
    id: int
    name: str
    habits: list[HabitOut]


class HabitList(BaseModel):
    id: int
    name: str


class HabitListRepository:
    def create(self, habit: HabitListIn, account_data: dict) -> HabitListOut:
        try:
            with pool.connection() as conn:
                with conn.cursor(row_factory=dict_row) as db:
                    db.execute(
                        """
                        INSERT INTO habit_list
                            (name, creator_id)
                        VALUES
                            (%s, %s)
                        RETURNING *;
                        """,
                        [habit.name, account_data["id"]],
                    )
                    return HabitListOut(**db.fetchone())
        except Exception as e:
            print(type(e))
            raise HTTPException(
                status_code=500,
                detail="Something went wrong, please try again later",
            )

    def get_all(self) -> list[HabitListOut]:
        try:
            with pool.connection() as conn:
                with conn.cursor(row_factory=dict_row) as db:
                    db.execute(
                        """
                        SELECT hl.id,
                               hl.name,
                               h.id habit_id,
                               h.description habit_description
                        FROM habit_list hl
                        LEFT JOIN habit h ON hl.id = h.list_id
                        ORDER BY hl.id;
                        """
                    )
                    habit_lists_dict = {}
                    rows = db.fetchall()
                    for row in rows:
                        list_id = row["id"]
                        list_name = row["name"]
                        habit_id = row["habit_id"]
                        habit_description = row["habit_description"]
                        if (
                            list_id in habit_lists_dict
                            and habit_id is not None
                        ):
                            # If it exists, append the habit
                            # to its list of habits
                            habit_lists_dict[list_id]["habits"].append(
                                HabitOut(
                                    id=habit_id, description=habit_description
                                )
                            )
                        else:
                            # If it doesn't exist, create a new
                            # entry with the habit list and its first habit
                            habit_lists_dict[list_id] = {
                                "id": list_id,
                                "name": list_name,
                                "habits": [],
                            }
                            if habit_id is not None:
                                habit_lists_dict[list_id]["habits"].append(
                                    HabitOut(
                                        id=habit_id,
                                        description=habit_description,
                                    )
                                )
                    return [
                        HabitListOutWithHabits(**row)
                        for row in list(habit_lists_dict.values())
                    ]
        except Exception as e:
            print(type(e))
            raise HTTPException(
                status_code=500,
                detail="Something went wrong, please try again later",
            )

    def get(self, username: str) -> HabitList:
        try:
            with pool.connection() as conn:
                with conn.cursor(row_factory=dict_row) as db:
                    db.execute(
                        """
                        SELECT * FROM habit
                        WHERE username = %s;
                        """,
                        [username],
                    )
                    return HabitList(**db.fetchone())
        except Exception as e:
            print(type(e))
            raise HTTPException(
                status_code=500,
                detail="Something went wrong, please try again later",
            )

    def get_one(self, username: int) -> HabitListOut:
        try:
            with pool.connection() as conn:
                with conn.cursor(row_factory=dict_row) as db:
                    db.execute(
                        """
                        SELECT * FROM habit
                        WHERE id = %s;
                        """,
                        [id],
                    )
                    return HabitListOut(**db.fetchone())
        except Exception as e:
            print(type(e))
            raise HTTPException(
                status_code=500,
                detail="Something went wrong, please try again later",
            )

    def update(self, id: int, habit: HabitListIn) -> HabitListOut:
        habit_data = habit.dict()
        values = list(habit_data.values())
        with pool.connection() as conn:
            try:
                with conn.cursor(row_factory=dict_row) as db:
                    db.execute(
                        """
                        UPDATE habit
                        SET username = %s
                            , hashed_password = %s
                            , email = %s
                        WHERE id = %s
                        RETURNING *;
                        """,
                        [*values, id],
                    )
                    result = db.fetchone()
                    if result is None:
                        raise HTTPException(
                            status_code=404, detail="Habit not found"
                        )
                        return HabitListOut(**db.fetchone())
            except Exception as e:
                print(type(e))
                raise HTTPException(
                    status_code=404,
                    detail="Update failed, please try again later",
                )

    def delete(self, id: int) -> bool:
        try:
            with pool.connection() as conn:
                with conn.cursor(row_factory=dict_row) as db:
                    db.execute(
                        """
                        DELETE FROM habit
                        WHERE id = %s;
                        """,
                        [id],
                    )
                    if db.rowcount == 0:
                        raise HTTPException(
                            status_code=404, detail="Habit not found"
                        )
                    return True
        except Exception as e:
            print(type(e))
            raise HTTPException(
                status_code=500,
                detail="Something went wrong, please try again later",
            )
