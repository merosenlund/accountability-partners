from queries.messages import MessageIn, MessageRepository, MessageOut
from fastapi import APIRouter, WebSocket, WebSocketDisconnect, Depends
from typing import List
import json
from datetime import datetime, timezone


router = APIRouter()


def timestamp():
    return datetime.now(timezone.utc).isoformat()


class ConnectionManager:
    def __init__(self):
        self.active_connections: dict[WebSocket] = {}
        self.current_message_id = 0

    async def connect(
        self,
        websocket: WebSocket,
        list_id: int,
        client_id: int,
    ):
        print("############################# connect", list_id, client_id)
        await websocket.accept()
        connections_for_list = self.active_connections.get(list_id, [])
        connections_for_list.append(websocket)
        self.active_connections[list_id] = connections_for_list
        await self.send_personal_message(
            "handshake",
            "Connected",
            list_id,
            client_id,
            websocket,
        )
        print(
            "active connections:",
            self.active_connections.get(list_id, []),
        )

    def disconnect(self, websocket: WebSocket):
        self.active_connections.remove(websocket)

    async def send_personal_message(
        self,
        message_type: str,
        message: str,
        list_id: int,
        client_id: int,
        websocket: WebSocket,
    ):
        payload = json.dumps(
            {
                "list_id": list_id,
                "client_id": client_id,
                "message_type": message_type,
                "body": message,
                "timestamp": timestamp(),
                "message_id": self.next_message_id(),
            }
        )
        await websocket.send_text(payload)

    async def broadcast(
        self,
        message_type: str,
        message: MessageOut,
    ):
        payload = json.dumps(
            {
                "client_id": message.sent_by,
                "message_type": message_type,
                "channel": message.list_id,
                "content": {
                    "id": message.id,
                    "body": message.body,
                    "sent_at": message.sent_at,
                    "sent_by": message.sent_by,
                    "list_id": message.list_id,
                    "viewed": message.viewed,
                },
                "sent_at": timestamp(),
                "message_id": self.next_message_id(),
            }
        )
        for connection in self.active_connections.get(message.list_id, []):
            await connection.send_text(payload)

    def next_message_id(self):
        self.current_message_id += 1
        return self.current_message_id


manager = ConnectionManager()


@router.websocket("/messages/{list_id}/{client_id}")
async def messages_websocket(
    websocket: WebSocket,
    list_id: int,
    client_id: int,
    repo: MessageRepository = Depends(),
):
    await manager.connect(websocket, list_id, client_id)
    try:
        while True:
            raw_message = await websocket.receive_text()
            message = repo.create(
                MessageIn(
                    body=raw_message, sent_by_id=client_id, list_id=list_id
                )
            )
            await manager.broadcast("message", message)
    except WebSocketDisconnect:
        manager.disconnect(websocket)
        await manager.broadcast("disconnected", "Disconnected", client_id)


@router.get("/messages/{list_id}")
def get_messages(
    list_id: int, repo: MessageRepository = Depends()
) -> List[MessageOut]:
    return repo.get_all(list_id)
