from fastapi import APIRouter, Depends, Request, Response
from queries.accounts import AccountIn, AccountOut, AccountRepository
from authenticator import authenticator
from jwtdown_fastapi.authentication import Token
from pydantic import BaseModel


class AccountForm(BaseModel):
    username: str
    password: str


class AccountToken(Token):
    account: AccountOut


router = APIRouter()


@router.get("/token", response_model=AccountToken | None)
async def get_token(
    request: Request,
    account: AccountIn = Depends(authenticator.try_get_current_account_data),
) -> AccountToken | None:
    if account and authenticator.cookie_name in request.cookies:
        return {
            "access_token": request.cookies[authenticator.cookie_name],
            "type": "Bearer",
            "account": account,
        }


@router.post("/accounts", status_code=201)
async def create_account(
    info: AccountIn,
    request: Request,
    response: Response,
    repo: AccountRepository = Depends(),
) -> AccountToken:
    hashed_password = authenticator.hash_password(info.password)
    account = repo.create(info, hashed_password)
    form = AccountForm(username=info.email, password=info.password)
    token = await authenticator.login(response, request, form, repo)
    return AccountToken(account=account, **token.dict())


@router.get("/accounts")
def get_accounts(repo: AccountRepository = Depends()) -> list[AccountOut]:
    users = repo.get_all()
    return users


@router.put("/accounts/{account_id}")
def update_account(
    account_id: int, account: AccountIn, repo: AccountRepository = Depends()
) -> AccountOut:
    return repo.update(account_id, account)


@router.delete("/accounts/{account_id}")
def delete_account(
    account_id: int, repo: AccountRepository = Depends()
) -> bool:
    repo.delete(account_id)
    return True


@router.get("/accounts/{account_id}")
def get_account(
    account_id: int, repo: AccountRepository = Depends()
) -> AccountOut:
    return repo.get_one(account_id)
