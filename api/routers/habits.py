from fastapi import APIRouter, Depends, Request, Response
from queries.habits import HabitIn, HabitOut, HabitRepository


router = APIRouter()


@router.post("/habits", status_code=201)
async def create_habit(
    info: HabitIn,
    request: Request,
    response: Response,
    repo: HabitRepository = Depends(),
) -> HabitOut:
    habit = repo.create(info)
    return HabitOut(**habit.dict())


@router.get("/habits")
def get_habits(
    request: Request,
    repo: HabitRepository = Depends(),
    # account_data: dict = Depends(authenticator.get_current_account_data),
) -> list[HabitOut]:
    habits = repo.get_all()
    print(habits)
    return habits


@router.patch("/habits/{habit_id}")
def patch_habit(
    habit_id: int, habit: HabitIn, repo: HabitRepository = Depends()
) -> HabitOut:
    return repo.update(habit_id, habit)


@router.put("/habits/{habit_id}")
def update_habit(
    habit_id: int, habit: HabitIn, repo: HabitRepository = Depends()
) -> HabitOut:
    return repo.update(habit_id, habit)


@router.delete("/habits/{habit_id}")
def delete_habit(habit_id: int, repo: HabitRepository = Depends()) -> bool:
    repo.delete(habit_id)
    return True


@router.get("/habits/{habit_id}")
def get_habit(habit_id: int, repo: HabitRepository = Depends()) -> HabitOut:
    return repo.get_one(habit_id)
