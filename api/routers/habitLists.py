from fastapi import APIRouter, Depends, Request, Response
from queries.habitLists import (
    HabitListIn,
    HabitListOut,
    HabitListRepository,
    HabitListOutWithHabits,
)
from authenticator import authenticator


router = APIRouter()


@router.post("/lists", status_code=201)
async def create_habit_list(
    info: HabitListIn,
    request: Request,
    response: Response,
    repo: HabitListRepository = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
) -> HabitListOut:
    habit = repo.create(info, account_data)
    return HabitListOut(**habit.dict())


@router.get("/lists")
def get_lists(
    request: Request,
    repo: HabitListRepository = Depends(),
    # account_data: dict = Depends(authenticator.get_current_account_data),
) -> list[HabitListOutWithHabits]:
    lists = repo.get_all()
    return lists


@router.put("/lists/{habit_id}")
def update_habit(
    habit_id: int, habit: HabitListIn, repo: HabitListRepository = Depends()
) -> HabitListOut:
    return repo.update(habit_id, habit)


@router.delete("/lists/{habit_id}")
def delete_habit(habit_id: int, repo: HabitListRepository = Depends()) -> bool:
    repo.delete(habit_id)
    return True


@router.get("/lists/{habit_id}")
def get_habit(
    habit_id: int, repo: HabitListRepository = Depends()
) -> HabitListOut:
    return repo.get_one(habit_id)
