steps = [
    [
        # "Up" SQL statement
        """
        ALTER TABLE users RENAME TO account;
        """,
        # "Down" SQL statement
        """
        ALTER TABLE account RENAME TO users;
        """,
    ]
]
