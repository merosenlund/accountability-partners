steps = [
    [
        # "Up" SQL statement
        """
        CREATE TABLE habit_list (
            id SERIAL PRIMARY KEY,
            name VARCHAR(255) NOT NULL,
            creator_id INTEGER NOT NULL REFERENCES "account" (id) ON DELETE CASCADE,
            buddy_id INTEGER REFERENCES "account" (id) ON DELETE CASCADE
        );

        CREATE TABLE habit_log (
            id SERIAL PRIMARY KEY,
            habit_id INTEGER NOT NULL REFERENCES habit_list (id) ON DELETE CASCADE,
            completed_by INTEGER NOT NULL REFERENCES "account" (id) ON DELETE CASCADE,
            completed_at TIMESTAMP NOT NULL DEFAULT NOW()
        );

        CREATE TABLE message (
            id SERIAL PRIMARY KEY,
            body TEXT NOT NULL,
            sent_at TIMESTAMP NOT NULL DEFAULT NOW(),
            sent_by INTEGER NOT NULL REFERENCES "account" (id) ON DELETE CASCADE,
            list_id INTEGER NOT NULL REFERENCES habit_list (id) ON DELETE CASCADE,
            viewed BOOLEAN NOT NULL DEFAULT FALSE
        );

        ALTER TABLE habit ADD COLUMN list_id INTEGER REFERENCES habit_list (id) ON DELETE CASCADE;
        """,
        # "Down" SQL statement
        """
        DROP TABLE habit_list;

        DROP TABLE habit_log;

        DROP TABLE message;

        ALTER TABLE habit DROP COLUMN list_id;
        """,
    ]
]
