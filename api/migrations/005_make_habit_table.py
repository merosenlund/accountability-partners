steps = [
    [
        # "Up" SQL statement
        """
        CREATE TABLE habit (
            id SERIAL PRIMARY KEY,
            description VARCHAR(255) NOT NULL
        );
        """,
        # "Down" SQL statement
        """
        DROP TABLE habit;
        """,
    ]
]
