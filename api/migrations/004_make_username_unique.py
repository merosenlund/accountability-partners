steps = [
    [
        # "Up" SQL statement
        """
        ALTER TABLE account ADD CONSTRAINT unique_username UNIQUE (username);
        """,
        # "Down" SQL statement
        """
        ALTER TABLE account DROP CONSTRAINT unique_username;
        """
    ]
]
