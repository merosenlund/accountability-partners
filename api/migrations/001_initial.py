steps = [
    [
        # "Up" SQL statement
        """
        CREATE TABLE user_ (
            id SERIAL PRIMARY KEY NOT NULL,
            username VARCHAR(1000) NOT NULL,
            email VARCHAR(1000) NOT NULL,
            hashed_password VARCHAR(1000) NOT NULL
        );
        """,
        # "Down" SQL statement
        """
        DROP TABLE user_;
        """,
    ]
]
