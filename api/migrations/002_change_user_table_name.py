steps = [
    [
        # "Up" SQL statement
        """
        ALTER TABLE user_ RENAME TO users;
        """,
        # "Down" SQL statement
        """
        ALTER TABLE users RENAME TO user_;
        """,
    ]
]
